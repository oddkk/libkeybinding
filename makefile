LIBDIR=./libkeybindings/
TESTDIR=./test/

all: lib test

lib:
	@echo Making library
	@make -C $(LIBDIR)

.PHONY:lib

test:
	@echo Making test
	@make -C $(TESTDIR)
	@echo Running test
	@./tests

.PHONY:test

lib-clean:
	@echo Cleaning lib
	@make -C $(LIBDIR) clean

.PHONY:lib-clean

test-clean:
	@echo Cleaning test
	@make -C $(TESTDIR) clean

.PHONY:test-clean


clean:
	@echo Cleaning all
clean:lib-clean test-clean

.PHONY:clean

lib-rebuild:
	@echo Rebuilding lib
	@make -C $(LIBDIR) rebuild

.PHONY:lib-rebuild

test-rebuild:
	@echo Rebuilding test
	@make -C $(TESTDIR) rebuild

.PHONY:test-rebuild


rebuild:
	@echo Rebuilding all
rebuild:lib-rebuild test-rebuild

.PHONY:rebuild

