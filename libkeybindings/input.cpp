#include "input.h"
#include <string.h>

int Input::_nextDeviceId = 0;
const float Input::axisTriggerPoint = 0.5f;
const float Input::axisReleasePoint = 0.25f;
const float Input::axisDeadzone = 0.05f;
std::map<std::string, InputDevice*> Input::devices;


Axis::Axis()
	: _next(0.0f), _current(0.0f), _previous(0.0f)
{}

Button::Button()
	: _next(false), _current(false), _previous(false)
{}

void Axis::tick()
{
	_previous = _current;
	_current = _next;
}

void Axis::set(float state) {
	_next = state;
}

static float min(float f1, float f2) {
	return f1 < f2 ? f1 : f2;
}

static float max(float f1, float f2) {
	return f1 > f2 ? f1 : f2;
}

static float clamp(float n, float min, float max) {
	return ::min(::max(n, max), min);
}

static float calculateAxisMode(float n, AxisMode mode)
{
	switch (mode)
	{
	case AxisMode::positive:
		return clamp(n, 0.0f, 1.0f);
	case AxisMode::negative:
		return clamp(n * -1.0f, 0.0f, 1.0f);
	case AxisMode::full:
		return clamp(n, -1.0f, 1.0f);
	}
	return n;
}

float Axis::state(AxisMode mode) const {
	return calculateAxisMode(_current, mode);
}


bool Axis::down(AxisMode mode) const {
	return calculateAxisMode(_current, mode) > Input::axisTriggerPoint;
}

bool Axis::downThisTick(AxisMode mode) const {
	return calculateAxisMode(_current, mode) > Input::axisTriggerPoint &&
		calculateAxisMode(_previous, mode) < Input::axisTriggerPoint;
}

bool Axis::upThisTick(AxisMode mode) const {
	return calculateAxisMode(_current, mode) < Input::axisTriggerPoint &&
		calculateAxisMode(_previous, mode) > Input::axisTriggerPoint;
}

void Button::tick() {
	_previous = _current;
	_current = _next;
}

void Button::set(bool state) {
	_next = state;
}

bool Button::down() const {
	return _current;
}

bool Button::downThisTick() const {
	return _current && !_previous;
}

bool Button::upThisTick() const {
	return !_current && _previous;
}

void InputDevice::update() {
	for (Axis& a : axis)
		a.tick();
	for (Button& b : buttons)
		b.tick();
	_update();
}

void Input::registerDevice(InputDevice* dev)
{
	dev->devId = _nextDeviceId++;
	devices.emplace(dev->name(), dev);
}

InputDevice* Input::getDevice(std::string name)
{
	auto iter = devices.find(name);
	if (iter != devices.end())
		return iter->second;
	return nullptr;
}

void Input::tick()
{
	for (auto iter = devices.begin(); iter != devices.end(); iter++)
	{
		iter->second->update();
	}
}

