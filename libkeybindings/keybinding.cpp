#include "keybinding.h"
#include "input.h"
#include "keyparser.h"

std::map<std::string, Binding*> Keybinding::_bindings;

Binding::Binding(std::string name)
	: name(name)
{
	Keybinding::_registerBinding(this);
}

bool Binding::down() const {
	for (unsigned int i = 0; i < _bound.size(); i++)
	{
	if (_bound[i].down())
			return true;
	}
	return false;
}

bool Binding::downThisTick() const {
	bool found = false;
	for (unsigned int i = 0; i < _bound.size(); i++)
	{
		if (_bound[i].downThisTick())
			found = true;
		else if (_bound[i].down())
			return false;
	}
	return found;
}

bool Binding::upThisTick() const {
	bool found = false;
	for (unsigned int i = 0; i < _bound.size(); i++)
	{
		if (_bound[i].upThisTick())
			found = true;
		if (_bound[i].down())
			return false;
	}
	return found;
}

static float max(float f1, float f2) {
	return f1 > f2 ? f1 : f2;
}

float Binding::axis() const {
	float max = 0.0f;
	for (unsigned int i = 0; i < _bound.size(); i++)
	{
		max = ::max(max, _bound[i].axis());
	}
	return max;
}

void Binding::bind(InputChannel cnl) {
	_bound.push_back(cnl);
}

DirectionalBinding::DirectionalBinding(std::string name)
	: up(name + "_up"),
	down(name + "_down"),
	left(name + "_left"),
	right(name + "_right")
{}

float DirectionalBinding::getXAxis() const {
	return right.axis() - left.axis();
}

float DirectionalBinding::getYAxis() const {
	return up.axis() - down.axis();
}

void Keybinding::_registerBinding(Binding* binding) {
	_bindings.insert(std::pair<std::string, Binding*>(binding->name, binding));
}

Binding* Keybinding::getBindingByName(std::string name)
{
	auto iter = _bindings.find(name);
	if (iter != _bindings.end())
		return iter->second;
	return nullptr;
}

static AxisMode axisModeFromString(std::string str)
{
	if (str == "positive" || str == "+") return AxisMode::positive;
	else if (str == "negative" || str == "-") return AxisMode::negative;
	else return AxisMode::full;
}

static ChannelType channelTypeFromString(std::string str)
{
	return str == "axis" ? ChannelType::axis : ChannelType::button;
}

