#include "input.h"

InputChannel::InputChannel(int channel, std::string device, ChannelType type, AxisMode mode)
	: _channel(channel), _device(Input::getDevice(device)), _type(type), _mode(mode)
{}

InputChannel::InputChannel(int channel, InputDevice* device, ChannelType type, AxisMode mode)
	: _channel(channel), _device(device), _type(type), _mode(mode)
{}

std::string InputChannel::devName() const {
	return _device->name();
}

std::string InputChannel::name() const {
	return "not implemented";
}

ChannelType InputChannel::type() const {
	return _type;
}

int InputChannel::id() const {
	return _channel;
}

static bool between(int n, int min, int max) {
	return min <= n && n < max;
}

bool InputChannel::down() const {
	if (_type == ChannelType::button &&
		between(_channel, 0, _device->buttons.size()))
		return _device->buttons[_channel].down();
	if (_type == ChannelType::axis &&
		between(_channel, 0, _device->axis.size()))
		return _device->axis[_channel].down(_mode);
	return false;
}

float InputChannel::axis() const {
	if (_type == ChannelType::button &&
		between(_channel, 0, _device->buttons.size()))
		return _device->buttons[_channel].down() ? 1.0f : 0.0f;
	if (_type == ChannelType::axis &&
		between(_channel, 0, _device->axis.size()))
		return _device->axis[_channel].state(_mode);
	return 0.0f;
}

bool InputChannel::downThisTick() const {
	if (_type == ChannelType::button &&
		between(_channel, 0, _device->buttons.size()))
		return _device->buttons[_channel].downThisTick();
	if (_type == ChannelType::axis &&
		between(_channel, 0, _device->axis.size()))
		return _device->axis[_channel].downThisTick(_mode);
	return false;
}

bool InputChannel::upThisTick() const {
	if (_type == ChannelType::button &&
		between(_channel, 0, _device->buttons.size()))
		return _device->buttons[_channel].upThisTick();
	if (_type == ChannelType::axis &&
		between(_channel, 0, _device->axis.size()))
		return _device->axis[_channel].upThisTick(_mode);
	return false;
}
